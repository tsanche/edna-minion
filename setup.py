import io
import os
from setuptools import setup, find_packages


def read(*paths, **kwargs):
    '''Read the contents of a text file safely.
    >>> read('project_name', 'VERSION')
    '0.1.0'
    >>> read('README.md')
    ...
    '''

    content = ''
    with io.open(
            os.path.join(os.path.dirname(__file__), *paths),
            encoding=kwargs.get('encoding', 'utf8'),
    ) as open_file:
        content = open_file.read().strip()
    return content


def read_requirements(path):
    return [line.strip() for line in read(path).split('\n') if not line.startswith(('""', '#', '-', 'git+'))]


setup(
    name='edna_minion_pipeline',
    author='Théophile Sanchez',
    author_email='theophile.sanchez@gmail.com',
    long_description=read('README.md'),
    long_description_content_type='text/markdown',
    version='1.0',
    packages=find_packages(),
    entry_points={'console_scripts': [
        'edna_minion_pipeline=edna_minion_pipeline:main',
    ]},
    install_requires=read_requirements('requirements.txt'),
)
