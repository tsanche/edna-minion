# eDNA MinION
Welcome to the eDNA MinION Repository!

This repository contains the eDNA MinION pipeline and a Docker image (`edna_minion_tools`), which includes all the necessary packages for seamless execution. The pipeline integrates several third-party bioinformatics tools to perform taxonomic assignation of MinION sequencing data. By using a single configuration file, it improves reproducibility and simplifies setup. It also incorporates an additional layer of parallelization, which is particularly useful when processing multiple samples simultaneously. Additionally, it generates plots for quality control and reporting.

The pipeline consists in 5 steps:
- Basecalling using [`dorado`](https://github.com/nanoporetech/dorado)
- Demultiplexing using [`dorado`](https://github.com/nanoporetech/dorado)
- Clustering using [`amplicon_sorter`](https://github.com/avierstr/amplicon_sorter)
- Taxonomic assignment with [`BLAST+`](https://blast.ncbi.nlm.nih.gov/doc/blast-help/downloadblastdata.html)
- Reporting and result filtering with custom python scripts

## eDNA MinION pipeline
This pipline performs taxonomic assignation starting from a folder containing pod5 files and a TOML configuration file (you can find an example in `config/example_config.toml`). A reference database in fasta format should be first prepared using the `makeblastdb` command from `BLAST+` (available in the docker image). 

To run the pipeline you can use the docker image with all the dependencies already installed with:
`docker run -it --rm --gpus all -v $HOME:$HOME -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro --user $(id -u):$(id -g) thesanc/edna_minion_tools -c config/example_config.toml`

Another option is to first clone this repository. Then install the python packages in `requirements.txt` and the softwares listed in the "Docker image" section. Finally, you can use the following command:
`python3 edna_minion_pipeline.py -c config/example_config.toml`

## Setup instructions for the docker image
To utilize the Docker image effectively, please ensure you have [docker](https://docs.docker.com/engine/install/) and the [NVIDIA Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html) installed beforehand.
To start the container and avoid permission issues with newly created files, we recommend using the following command:
`docker run -it --rm --gpus all -v $HOME:$HOME -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro --user $(id -u):$(id -g) thesanc/edna_minion_tools bash`

## Docker image `edna_minion_tools` 
The Docker image `edna_minion_tools` is available [here](https://hub.docker.com/r/thesanc/edna_minion_tools) and contains the following packages:
- `python` 3.10.12
- `dorado` 0.7.2
- `chopper` 0.7.0
- `amplicon_sorter`
- `ncbi-blast+` 2.15.0
- `samtools` 1.19.2
- `BLAST+` 2.15.0
- `KronaTools` 2.8.1

## To-do list
- catch errors returned by the bash commands
- integrate the scripts into Nextflow
- add unitary tests
- add configuration file check
- add plots for quality control and follow the number of reads filtered out
