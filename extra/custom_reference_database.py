import pandas as pd
import re
import numpy as np
import os
from tqdm import tqdm
from Bio.SeqIO.FastaIO import SimpleFastaParser
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO

midori2_co1_path = 'data/reference_database/CO1/MIDORI2_TOTAL_SP_NUC_GB259_CO1_RAW.fasta'
midori2_cytB_path = 'data/reference_database/cytb/MIDORI2_TOTAL_SP_NUC_GB259_Cytb_RAW.fasta'
bold_path = 'data/reference_database/bold/BOLD_Public.21-Jun-2024.fasta'
bold_metadata_path = 'data/reference_database/bold/BOLD_Public.21-Jun-2024.tsv'
selected_markers_in_bold = r'coi|co1|cytb|rbcl'
taxonomic_keys = ['kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']

output_path = 'data/reference_database/custom_base/custom_base.fasta'


### Processing Midori2
def midori_to_dataframe(input_fasta_path, marker_code):
    keys = ['id', 'sequence', 'bin_uri', 'marker_code', 'database_name'] + taxonomic_keys
    all_fasta = {k: [] for k in keys}
    with open(input_fasta_path) as handle:
        for id, sequence in tqdm(SimpleFastaParser(handle)):
            description = id.split(';')
            all_fasta['id'].append(description[0])
            all_fasta['sequence'].append(sequence)
            all_fasta['bin_uri'].append(None)
            all_fasta['marker_code'].append(marker_code)
            all_fasta['database_name'].append(os.path.basename(input_fasta_path))
            for key in taxonomic_keys:
                taxo = [string for string in description if re.search(f'^{key}_', string, re.IGNORECASE)]
                if len(taxo) == 1:
                    all_fasta[key].append(taxo[0].split('_')[1])
                else:
                    all_fasta[key].append(None)
    return pd.DataFrame(all_fasta)


midori_cytb = midori_to_dataframe(midori2_cytB_path, 'cytB')
midori_co1 = midori_to_dataframe(midori2_co1_path, 'CO1')

### Processing BOLD
bold_metadata = pd.read_csv(bold_metadata_path, sep='\t', on_bad_lines='skip')
print(f'{15109730-len(bold_metadata)} rows lost because of bad formating')
bold_metadata['marker_code'] = bold_metadata['marker_code'].astype(str)
selected_marker = np.array(
    [bool(re.search(selected_markers_in_bold, string, re.IGNORECASE)) for string in bold_metadata['marker_code']])
selected_marker = selected_marker.astype(bool)
bold_metadata = bold_metadata[selected_marker]
print(f'{len(bold_metadata)} rows remaining after filtering the markers')

bold_metadata = bold_metadata[[
    'processid', 'marker_code', 'bin_uri', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species'
]].rename(columns={'processid': 'id'})

with open(bold_path) as handle:
    bold_fasta = {k: [] for k in ['id', 'marker_code', 'sequence', 'database_name']}
    for id, sequence in tqdm(SimpleFastaParser(handle)):
        info = id.split('|')
        if bool(re.search(selected_markers_in_bold, info[1], re.IGNORECASE)):
            bold_fasta['id'].append(info[0])
            bold_fasta['marker_code'].append(info[1])
            bold_fasta['sequence'].append(sequence)
            bold_fasta['database_name'].append(os.path.basename(bold_path))

bold_fasta = pd.DataFrame(bold_fasta)
bold_data = pd.merge(bold_fasta, bold_metadata, how='outer', on=['id', 'marker_code'])

### Merging and filtering duplicates
all_reference_db = pd.concat((bold_data, midori_co1, midori_cytb))
all_reference_db = all_reference_db.reset_index()
all_reference_db = all_reference_db.drop(all_reference_db[all_reference_db['sequence'] == ''].index)

all_reference_db_grouped = all_reference_db.groupby('sequence')
seq_records = []
for name, group in tqdm(all_reference_db_grouped):
    if len(group) > 1:
        if not group['bin_uri'].isnull().all():
            group = group.drop(group[group['bin_uri'].isnull()].index)

    row = group.iloc[0].replace({np.nan: None})
    info = f'{row["id"]};{row["marker_code"]};{row["database_name"]};{row["bin_uri"]};{row["kingdom"]};{row["phylum"]};{row["class"]};{row["order"]};{row["family"]};{row["genus"]};{row["species"]}'
    seq_record = SeqRecord(Seq(row['sequence']), id=info, description="")
    seq_records.append(seq_record)

SeqIO.write(seq_records, output_path, "fasta")
