import os
import logging
import subprocess
import re
import toml
import pandas as pd
from contextlib import contextmanager


@contextmanager
def execute_in_directory(new_path: str):
    old_path = os.getcwd()
    try:
        os.chdir(new_path)
        yield

    finally:
        os.chdir(old_path)


def run_command(command: str):
    logging.info(f'Executing command {command}')
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    return stdout, stderr


def count_sequences_in_fasta_fastq(file_path: str):
    count = 0
    with open(file_path, 'rb') as file:
        first_char = file.read(1)
        file.seek(0)

        if first_char == b'>':
            for line in file:
                if line.startswith(b'>'):
                    count += 1
        elif first_char == b'@':
            for line_number, line in enumerate(file):
                if line_number % 4 == 0:
                    if line.startswith(b'@'):
                        count += 1
    return count


def number_formater(string):
    parts = re.split(r'(\d+)', string)
    for i, part in enumerate(parts):
        if part.isdigit():
            parts[i] = int(part)
    return parts


def check_config(config: dict):
    if 'basecalling_model' not in config['basecalling'].keys():
        config['basecalling']['basecalling_model'] = None
    if 'basecalling_model_path' not in config['basecalling'].keys():
        config['basecalling']['basecalling_model_path'] = None
    if 'barcode_csv_file' not in config['demultiplexing'].keys(
    ) and 'kit-name' in config['demultiplexing']['arguments'].keys():
        config['demultiplexing']['barcode_csv_file'] = None
    if 'reference_database' not in config.keys():
        config['reference_database'] = {}
    if 'format' not in config['reference_database'].keys():
        config['reference_database']['format'] = None
    if 'separator' not in config['reference_database'].keys():
        config['reference_database']['separator'] = None
    if 'max_target_seqs' not in config['taxonomic_assignment']['arguments'].keys():
        config['taxonomic_assignment']['arguments']['max_target_seqs'] = 1
    if 'start_date' not in config['global'].keys():
        config['global']['start_date'] = None
    if 'process_unclustered_read' not in config['taxonomic_assignment'].keys():
        config['taxonomic_assignment']['process_unclustered_read'] = False
    if 'phylogenetic_thresholds' not in config.keys():
        config['phylogenetic_thresholds'] = None


def save_config(config: dict, resume_value: bool, start_date):
    if config['global']['start_date'] is None or not resume_value:
        config['global']['start_date'] = start_date
    run_folder = f'{config["global"]["run_name"]}_{config["global"]["start_date"]}'
    run_path = os.path.join(config['global']['output_path'], run_folder)
    if resume_value:
        logging.info(f'Resuming run from {run_folder}')
    else:
        if not os.path.exists(run_path):
            logging.info(f'Creating output path in {run_path}')
            os.makedirs(run_path)
        config_file_path = os.path.join(run_path, f'{run_folder}_config.toml')
        with open(config_file_path, 'w') as config_file:
            logging.info(f'Copying config file to {config_file_path}')
            toml.dump(config, config_file)

    return run_folder, run_path


def load_config(config_file: str, resume_value: bool, start_date):
    with open(config_file, 'r') as config_file:
        config = toml.load(config_file)
    check_config(config)
    run_folder, run_path = save_config(config, resume_value, start_date)

    return run_folder, run_path, config


def output_species_per_sample(data, output_file='species_per_sample.csv'):
    data = data.copy()
    data = data[data['keep_for_analysis'] == True]
    data['species'] = data['species'].fillna(data['genus']).fillna(data['family']).fillna(data['order']).fillna(
        data['class']).fillna(data['phylum']).fillna(data['kingdom'])

    species_per_sample = pd.DataFrame(0, columns=data['sample_barcode'].unique(), index=data['species'].unique())
    species_per_sample.index.name = 'species'
    species_grouped = data.groupby('species')
    for species, species_group in species_grouped:
        sample_grouped = species_group.groupby('sample_barcode')
        for sample_barcode, sample_group in sample_grouped:
            species_per_sample.loc[species, sample_barcode] = sample_group['nb_reads'].sum()
    species_per_sample = species_per_sample.sort_values('species')
    species_per_sample.to_csv(output_file)


def output_sequence_per_sample(data, output_file='sequence_per_sample.csv'):
    data = data.copy()
    data = data[data['keep_for_analysis'] == True]

    sequence_per_sample = pd.DataFrame(0, columns=data['sample_barcode'].unique(), index=data['qseq'].unique())
    qseq_grouped = data.groupby('qseq')
    for qseq, qseq_group in qseq_grouped:
        sample_grouped = qseq_group.groupby('sample_barcode')
        for sample_barcode, sample_group in sample_grouped:
            sequence_per_sample.loc[qseq, sample_barcode] = sample_group['nb_reads'].sum()
    sequence_per_sample.to_csv(output_file)


def output_motu_as_fasta(data, output_file='motu.fasta'):
    data = data.copy()
    sequences_fasta = {}
    data = data[data['keep_for_analysis'] == True]
    motu_seq_grouped = data.groupby('motu_sequence')
    for motu_sequence, motu_seq_group in motu_seq_grouped:
        sequences_fasta[motu_seq_group['qseqid'].iloc[0]] = motu_sequence

    with open(output_file, 'w') as fasta_file:
        for qseqid in sequences_fasta.keys():
            fasta_file.write(f'>{qseqid.split("(")[0]}\n')
            fasta_file.write(f'{sequences_fasta[qseqid]}\n')


def filter_phylo(data, thresholds):
    for phylo_level, threshold in thresholds.items():
        data.loc[pd.to_numeric(data['pident']) < float(threshold), phylo_level] = None
    return data
