import os
import logging
import pandas as pd
import tomli_w
from edna_minion_modules.utils import execute_in_directory


def generate_barcode_files(run_name: str, barcode_csv_file: str) -> list[str]:
    logging.info(f'Converting barcode file {barcode_csv_file} for demultiplexing')
    barcode = pd.read_csv(barcode_csv_file, names=['barcode_name', 'TAG_F', 'TAG_R', 'F_PRIMER_SEQ', 'R_PRIMER_SEQ'])
    f_primer_seq = barcode['F_PRIMER_SEQ'].unique()
    r_primer_seq = barcode['R_PRIMER_SEQ'].unique()
    if len(f_primer_seq) > 1 or len(r_primer_seq) > 1:
        raise ValueError(f'More than one forward or reverse primer in {barcode_csv_file}')
    f_primer_seq = f_primer_seq[0]
    r_primer_seq = r_primer_seq[0]
    barcode_table = pd.DataFrame(columns=['barcode_name', 'TAG_F_name', 'TAG_R_name'])
    barcode_table = []
    i = 1
    n_digits = len(str(len(barcode) + 1))

    with open(f'{run_name}_barcode.fasta', 'a') as fasta_output_file:

        for _, row in barcode.iterrows():
            fasta_output_file.write(f'>TAG_F_{i:0{n_digits}d}\n')
            fasta_output_file.write(f'{row["TAG_F"]}\n')
            fasta_output_file.write(f'>TAG_R_{i:0{n_digits}d}\n')
            fasta_output_file.write(f'{row["TAG_R"]}\n')
            new_row = {
                'barcode_name': [row['barcode_name']],
                'TAG_F_name': [f'TAG_F_{i:0{n_digits}d}'],
                'TAG_R_name': [f'TAG_R_{i:0{n_digits}d}']
            }
            barcode_table.append(pd.DataFrame(new_row))
            i += 1
    barcode_table = pd.concat(barcode_table, ignore_index=True)
    barcode_table.to_csv(f'{run_name}_barcode_table.csv', index=False)
    barcode_arrangement = {
        'arrangement': {
            'name': run_name,
            'kit': '',
            'mask1_front': '',
            'mask1_rear': f_primer_seq,
            'mask2_front': '',
            'mask2_rear': r_primer_seq,
            'barcode1_pattern': f'TAG_F_%0{n_digits}i',
            'barcode2_pattern': f'TAG_R_%0{n_digits}i',
            'first_index': 1,
            'last_index': i - 1
        }
    }
    with open(f'{run_name}_barcode.toml', mode='wb') as barcode_arrangement_file:
        tomli_w.dump(barcode_arrangement, barcode_arrangement_file)

    return f'{run_name}_barcode.fasta', f'{run_name}_barcode.toml', f'{run_name}_barcode_table.csv'


class Demultiplexing:

    def __init__(self,
                 run_name: str,
                 run_path: str,
                 basecalled_bam_file: str,
                 arguments: dict,
                 skip_step: bool = False,
                 barcode_csv_file: str = None,
                 demultiplexing_path: str = 'demultiplexing') -> None:

        self.skip_step = skip_step
        self.run_name = run_name
        self.run_path = run_path
        self.demultiplexing_path = demultiplexing_path
        if self.skip_step:
            logging.info('Skipping demultiplexing')
            self.barcode_table_file = None
        else:
            logging.info(f'Preparing demultiplexing')
            with execute_in_directory(self.run_path):
                if not os.path.exists(self.demultiplexing_path):
                    logging.info(f'Creating demultiplexing output path in "{self.demultiplexing_path}"')
                    os.makedirs(self.demultiplexing_path)

            arguments_string = ' '.join([f'--{arg} {arguments[arg]}' for arg in arguments])

            if 'kit-name' not in arguments.keys() and barcode_csv_file is None:
                with execute_in_directory(self.run_path):
                    logging.info('No standard or custom barcoding kit provided')
                    self.demultiplexing_command = f'dorado demux {basecalled_bam_file} {arguments_string} --output-dir {self.demultiplexing_path}'
                    self.barcode_table_file = None

            elif 'kit-name' not in arguments.keys() and barcode_csv_file is not None:
                barcode_csv_file = os.path.realpath(barcode_csv_file)
                with execute_in_directory(self.run_path):
                    logging.info(f'Creating symlink for barcode table ({barcode_csv_file})')
                    os.symlink(barcode_csv_file, f'{self.run_name}_barcode.csv')

                    if 'barcode-arrangement' in arguments.keys() and 'barcode-sequences' in arguments.keys():
                        self.demultiplexing_command = f'dorado demux {basecalled_bam_file} {arguments_string} --output-dir {self.demultiplexing_path}'

                    else:
                        self.barcode_sequences_file, self.barcode_arrangement_file, self.barcode_table_file = generate_barcode_files(
                            self.run_name, f'{self.run_name}_barcode.csv')
                        self.demultiplexing_command = f'dorado demux {basecalled_bam_file} {arguments_string} --barcode-arrangement {self.barcode_arrangement_file} --barcode-sequences {self.barcode_sequences_file} --output-dir {self.demultiplexing_path}'

            else:
                logging.info(f'Demultiplexing using standard barcoding kit ({arguments["kit-name"]})')
                with execute_in_directory(self.run_path):
                    self.demultiplexing_command = f'dorado demux {basecalled_bam_file} {arguments_string} --output-dir {self.demultiplexing_path}'
                self.barcode_table_file = None

            logging.info(f'Generated demultiplexing command line: {self.demultiplexing_command}')

    def run(self) -> str:
        with execute_in_directory(self.run_path):
            if self.skip_step:
                logging.info(f'Using demultiplexed data in "{self.demultiplexing_path}"')
            else:
                logging.info('Starting demultiplexing')
                os.system(self.demultiplexing_command)
            return self.demultiplexing_path, self.barcode_table_file
