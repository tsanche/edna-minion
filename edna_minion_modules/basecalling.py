import os
import logging
from edna_minion_modules.utils import execute_in_directory


class Basecalling:

    def __init__(self,
                 run_name: str,
                 run_path: str,
                 pod5_path: str,
                 basecalling_model: str,
                 basecalling_model_path: str,
                 arguments: dict,
                 skip_step: bool = False,
                 basecalling_output_file: str = None) -> None:

        self.skip_step = skip_step
        self.run_name = run_name
        self.run_path = run_path

        if self.skip_step:
            logging.info('Skipping basecalling')
            self.basecalling_output_file = basecalling_output_file

        else:
            logging.info('Preparing basecalling')
            pod5_path = os.path.realpath(pod5_path)
            if basecalling_model_path is not None:
                basecalling_model_path = os.path.realpath(basecalling_model_path)

            with execute_in_directory(self.run_path):
                logging.info(f'Creating symlink for data folder ({pod5_path})')
                pod5_symlink_path = 'pod5'
                os.symlink(pod5_path, pod5_symlink_path, target_is_directory=True)

                self.basecalling_path = 'basecalling'
                if not os.path.exists(self.basecalling_path):
                    logging.info(f'Creating basecalling output path in {self.basecalling_path}')
                    os.makedirs(self.basecalling_path)

                arguments_string = ' '.join([f'--{arg} {arguments[arg]}' for arg in arguments])

                self.basecalling_output_file = os.path.join(self.basecalling_path, f'{self.run_name}_basecalled.bam')

                if basecalling_model is None and basecalling_model_path is not None:
                    self.basecalling_commands = f'dorado basecaller {basecalling_model_path} pod5 {arguments_string} > {self.basecalling_output_file}'
                else:
                    self.basecalling_commands = f'dorado download --model {basecalling_model} && \
                        dorado basecaller {basecalling_model} pod5 {arguments_string} > {self.basecalling_output_file}'

                logging.info(f'Generated basecalling command line: {self.basecalling_commands}')

    def run(self) -> None:
        with execute_in_directory(self.run_path):
            if self.skip_step:
                logging.info(f'Using basecalled data in "{self.basecalling_output_file}"')
            else:
                logging.info('Starting basecalling')
                os.system(self.basecalling_commands)

            return self.basecalling_output_file
