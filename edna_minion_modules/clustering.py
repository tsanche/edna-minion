import os
import logging
import concurrent.futures
from edna_minion_modules.utils import execute_in_directory
from edna_minion_modules.utils import run_command
from edna_minion_modules.utils import count_sequences_in_fasta_fastq


class Clustering:

    def __init__(self,
                 run_name: str,
                 run_path: str,
                 demultiplexing_path: str,
                 max_workers: int,
                 maxreads_coef: int,
                 arguments: dict,
                 skip_step: bool = False,
                 clustering_path: str = 'clustering') -> None:

        logging.info(f'Preparing clustering')
        self.run_name = run_name
        self.run_path = run_path
        self.clustering_path = clustering_path
        self.skip_step = skip_step
        if self.skip_step:
            logging.info('Skipping clustering')
        else:
            self.max_workers = max_workers
            with execute_in_directory(self.run_path):
                if not os.path.exists(self.clustering_path):
                    logging.info(f'Creating clustering output path in "{self.clustering_path}"')
                    os.makedirs(self.clustering_path)
                demultiplexed_files = next(os.walk(demultiplexing_path), (None, None, []))[2]
                if 'unclassified.fastq' in demultiplexed_files and len(demultiplexed_files) > 1:
                    demultiplexed_files.remove('unclassified.fastq')
                elif 'unclassified.fastq' in demultiplexed_files:
                    logging.info(f'Only unclassified reads in "{self.clustering_path}"')

                arguments_string = ' '.join([f'--{arg} {arguments[arg]}' for arg in arguments])

                if 'maxreads' not in arguments:
                    self.clustering_commands = [
                        f'amplicon_sorter -i {os.path.join(demultiplexing_path, f)} --maxreads {maxreads_coef * count_sequences_in_fasta_fastq(os.path.join(demultiplexing_path, f))} -o {self.clustering_path} {arguments_string}'
                        for f in demultiplexed_files
                    ]
                else:
                    self.clustering_commands = [
                        f'amplicon_sorter -i {os.path.join(demultiplexing_path, f)} -o {self.clustering_path} {arguments_string}'
                        for f in demultiplexed_files
                    ]

    def run(self) -> str:
        with execute_in_directory(self.run_path):
            if self.skip_step:
                logging.info(f'Using clustered data in "{self.clustering_path}"')
            else:
                logging.info('Starting clustering')
                with concurrent.futures.ThreadPoolExecutor(max_workers=self.max_workers) as executor:
                    futures = [executor.submit(run_command, cmd) for cmd in self.clustering_commands]
                    for future in concurrent.futures.as_completed(futures):
                        result = future.result()
                        if len(result[0]) > 0:
                            logging.info(result[0].decode('utf-8'))
                        if len(result[1]) > 0:
                            logging.error(result[1].decode('utf-8'))

        return self.clustering_path
