import os
import logging
import pandas as pd
import numpy as np
import re
from Bio import SeqIO
from edna_minion_modules.visualization import *
from edna_minion_modules.utils import *


def extract_number_of_read(text):
    match = re.search(r'\((\d+)\)$', text)
    if match:
        return int(match.group(1))
    else:
        return None


class Reporting:

    def __init__(self, run_name: str, run_path: str, barcode_table_file: str, clustering_path: str,
                 taxonomic_assignment_path: str, phylogenetic_thresholds: dict, process_unclustered_read: bool,
                 database_fasta_file: str, blast_output_format: str, max_target_seqs: int, refdb_format: str,
                 refdb_separator: str) -> None:
        self.run_name = run_name
        self.run_path = run_path
        self.barcode_table_file = barcode_table_file
        self.taxonomic_assignment_path = taxonomic_assignment_path
        self.phylogenetic_thresholds = phylogenetic_thresholds
        self.process_unclustered_read = process_unclustered_read
        self.refdb_format = refdb_format
        self.refdb_separator = refdb_separator
        self.reporting_path = 'results'
        self.max_target_seqs = max_target_seqs
        logging.info(f'Preparing index for database in "{database_fasta_file}"')
        self.database_index = SeqIO.index(database_fasta_file, format='fasta')
        self.blast_output_format = blast_output_format.split()[1:]
        self.blast_output_format[-1] = self.blast_output_format[-1][:-1]

        with execute_in_directory(self.run_path):
            if not os.path.exists(self.reporting_path):
                logging.info(f'Creating reporting output path in {self.reporting_path}')
                os.makedirs(self.reporting_path)
            self.clustering_paths = [
                os.path.join(clustering_path, f.name, f'{f.name}_consensussequences.fasta')
                for f in os.scandir(clustering_path) if f.is_dir()
            ]
            if self.process_unclustered_read:
                self.clustering_paths = self.clustering_paths + [
                    os.path.join(clustering_path, f.name, f'{f.name}_nogroup_unique.fasta')
                    for f in os.scandir(clustering_path) if f.is_dir()
                ]
            self.clustering_paths = [file for file in self.clustering_paths if os.path.exists(file)]

    def run(self):
        with execute_in_directory(self.run_path):
            clustered_sequences = {}
            for clustering_path in self.clustering_paths:
                index = SeqIO.index(clustering_path, 'fasta')
                clustered_sequences.update(index)

            if self.barcode_table_file is not None:
                barcode_table = pd.read_csv(self.barcode_table_file)
            else:
                barcode_names = np.unique([
                    d.split('.')[0].split('_consensussequences')[0].split('_nogroup_unique')[0]
                    for d in os.listdir(self.taxonomic_assignment_path)
                    if os.path.isfile(os.path.join(self.taxonomic_assignment_path, d))
                ])
                barcode_table = pd.DataFrame({'barcode_name': barcode_names})
            barcode_table.index = np.arange(1, len(barcode_table) + 1)
            n_digits = len(str(len(barcode_table) + 1))
            all_taxonomic_assignment_tables = []
            for index, row in barcode_table.iterrows():
                barcode = row['barcode_name']
                if self.barcode_table_file is not None:
                    taxonomic_assignment_file_path = os.path.join(
                        self.taxonomic_assignment_path,
                        f'{self.run_name}_barcode{index:0{n_digits}d}_consensussequences.tsv')
                else:
                    taxonomic_assignment_file_path = os.path.join(self.taxonomic_assignment_path,
                                                                  f'{barcode}_consensussequences.tsv')

                if os.path.isfile(taxonomic_assignment_file_path):
                    taxonomic_assignment_table = pd.read_csv(taxonomic_assignment_file_path,
                                                             names=self.blast_output_format,
                                                             sep='\t')
                    taxonomic_assignment_table['blast_rank'] = taxonomic_assignment_table.groupby(
                        'qseqid').cumcount() + 1
                    taxonomic_assignment_table['run_name'] = self.run_name
                    if len(taxonomic_assignment_table) > 0:
                        if self.barcode_table_file is not None:
                            taxonomic_assignment_table = taxonomic_assignment_table.assign(
                                sample_barcode=f'{index:0{n_digits}d}')
                        else:
                            taxonomic_assignment_table = taxonomic_assignment_table.assign(sample_barcode=barcode)

                        if 'sseqid' in self.blast_output_format:
                            taxonomic_assignment_table[
                                'description_from_reference_database'] = taxonomic_assignment_table['sseqid'].apply(
                                    lambda x: self.database_index.get(x).description)
                            taxonomic_assignment_table['reference_database_sequence'] = taxonomic_assignment_table[
                                'sseqid'].apply(lambda x: str(self.database_index.get(x).seq))
                            taxonomic_assignment_table['motu_sequence'] = taxonomic_assignment_table['qseqid'].apply(
                                lambda x: str(clustered_sequences.get(x).seq))
                        if self.refdb_format is not None and self.refdb_separator is not None:
                            taxonomic_assignment_table[self.refdb_format] = taxonomic_assignment_table[
                                'description_from_reference_database'].str.split(self.refdb_separator, expand=True)
                            taxonomic_assignment_table = taxonomic_assignment_table.drop(
                                'description_from_reference_database', axis=1)
                        taxonomic_assignment_table['nb_reads'] = taxonomic_assignment_table['qseqid'].apply(
                            extract_number_of_read)
                        all_taxonomic_assignment_tables.append(taxonomic_assignment_table)
                    else:
                        logging.info(f'File {taxonomic_assignment_file_path} is empty.')

                else:
                    logging.info(f'There is no file {taxonomic_assignment_file_path}.')

            if self.process_unclustered_read:
                for index, row in barcode_table.iterrows():
                    barcode = row['barcode_name']
                    if self.barcode_table_file is not None:
                        taxonomic_assignment_file_path = os.path.join(
                            self.taxonomic_assignment_path,
                            f'{self.run_name}_barcode{index:0{n_digits}d}_nogroup_unique.tsv')
                    else:
                        taxonomic_assignment_file_path = os.path.join(self.taxonomic_assignment_path,
                                                                      f'{barcode}_nogroup_unique.tsv')

                    if os.path.isfile(taxonomic_assignment_file_path):
                        taxonomic_assignment_table = pd.read_csv(taxonomic_assignment_file_path,
                                                                 names=self.blast_output_format,
                                                                 sep='\t')
                        taxonomic_assignment_table['qseqid'] = taxonomic_assignment_table['qseqid'].astype(str)
                        taxonomic_assignment_table['blast_rank'] = taxonomic_assignment_table.groupby(
                            'qseqid').cumcount() + 1
                        taxonomic_assignment_table['run_name'] = self.run_name
                        if len(taxonomic_assignment_table) > 0:
                            if self.barcode_table_file is not None:
                                taxonomic_assignment_table = taxonomic_assignment_table.assign(
                                    sample_barcode=f'{index:0{n_digits}d}')
                            else:
                                taxonomic_assignment_table = taxonomic_assignment_table.assign(sample_barcode=barcode)

                            if 'sseqid' in self.blast_output_format:
                                taxonomic_assignment_table[
                                    'description_from_reference_database'] = taxonomic_assignment_table['sseqid'].apply(
                                        lambda x: self.database_index.get(x).description)
                                taxonomic_assignment_table['reference_database_sequence'] = taxonomic_assignment_table[
                                    'sseqid'].apply(lambda x: str(self.database_index.get(x).seq))
                                taxonomic_assignment_table['motu_sequence'] = taxonomic_assignment_table[
                                    'qseqid'].apply(lambda x: str(clustered_sequences.get(x).seq))
                            if self.refdb_format is not None and self.refdb_separator is not None:
                                taxonomic_assignment_table[self.refdb_format] = taxonomic_assignment_table[
                                    'description_from_reference_database'].str.split(self.refdb_separator, expand=True)
                                taxonomic_assignment_table = taxonomic_assignment_table.drop(
                                    'description_from_reference_database', axis=1)
                            taxonomic_assignment_table['nb_reads'] = 1
                            all_taxonomic_assignment_tables.append(taxonomic_assignment_table)
                        else:
                            logging.info(f'File {taxonomic_assignment_file_path} is empty.')

                    else:
                        logging.info(f'There is no file {taxonomic_assignment_file_path}.')

            all_results_path = os.path.join(self.reporting_path, f'{self.run_name}_results.tsv')
            logging.info(f'Saving final table in "{all_results_path}"')
            all_taxonomic_assignment_tables = pd.concat(all_taxonomic_assignment_tables, ignore_index=True, axis=0)
            if self.phylogenetic_thresholds is not None:
                logging.info(f'Filtering phylogeny using the thresholds: "{self.phylogenetic_thresholds}"')
                all_taxonomic_assignment_tables = filter_phylo(all_taxonomic_assignment_tables,
                                                               self.phylogenetic_thresholds)
            all_taxonomic_assignment_tables['keep_for_analysis'] = all_taxonomic_assignment_tables['blast_rank'] == 1
            all_taxonomic_assignment_tables = all_taxonomic_assignment_tables.replace('None', None)

            def check_ambiguous_hits(taxonomic_assignment_table):
                if len(taxonomic_assignment_table) > 1:
                    phylo = ['kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
                    best_bit_score = taxonomic_assignment_table['bitscore'].iloc[0]
                    ambiguous_hits = taxonomic_assignment_table[taxonomic_assignment_table['bitscore'] ==
                                                                best_bit_score].copy()
                    if len(ambiguous_hits) > 1:
                        same_phylo = np.all(
                            [ambiguous_hits[col][ambiguous_hits[col].notna()].nunique() < 2 for col in phylo])

                        if not same_phylo:
                            taxonomic_assignment_table['keep_for_analysis'] = False
                            combined_row = pd.DataFrame([[
                                ambiguous_hits[col].iloc[0] if ambiguous_hits[col].nunique() == 1 else None
                                for col in ambiguous_hits.columns
                            ]],
                                                        columns=ambiguous_hits.columns)

                            combined_row['keep_for_analysis'] = True
                            taxonomic_assignment_table = pd.concat([taxonomic_assignment_table, combined_row],
                                                                   ignore_index=True)

                return taxonomic_assignment_table

            all_taxonomic_assignment_tables = all_taxonomic_assignment_tables.groupby('qseqid').apply(
                check_ambiguous_hits)
            all_taxonomic_assignment_tables.to_csv(all_results_path, index=False, sep='\t')
            pie_charts_path = os.path.join(self.reporting_path, 'pie_charts.html')
            logging.info(f'Creating Krona pie charts in "{pie_charts_path}"')
            plot_phylogeny_pie_chart_krona(all_taxonomic_assignment_tables, output_file=pie_charts_path)
            logging.info(f'Adding supplementary output formats in "{self.reporting_path}"')
            output_species_per_sample(all_taxonomic_assignment_tables,
                                      os.path.join(self.reporting_path, 'species_per_sample.csv'))
            output_sequence_per_sample(all_taxonomic_assignment_tables,
                                       os.path.join(self.reporting_path, 'sequence_per_sample.csv'))
            output_motu_as_fasta(all_taxonomic_assignment_tables, os.path.join(self.reporting_path, 'motu.fasta'))
