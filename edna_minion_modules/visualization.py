import os
import pysam
import glob
import pandas as pd
import numpy as np
import plotly.express as px
import plotly.graph_objs as go
from Bio import SeqIO
import matplotlib.pyplot as plt
import pdb

template = 'plotly_white'


def convert_char_to_phred_score(quality_string):
    phred_scores = [ord(char) - 33 for char in quality_string]
    return phred_scores


def load_reads(file_path):
    sequences = []
    qualities = []
    if file_path.lower().endswith('.fastq'):
        with open(file_path, 'r') as fastq_file:
            for record in SeqIO.parse(fastq_file, 'fastq'):
                sequences.append(str(record.seq))
                qualities.append(record.letter_annotations['phred_quality'])

    elif file_path.lower().endswith('.bam'):
        with open(file_path, 'rb') as bam_file:
            bam_data = pysam.AlignmentFile(bam_file, 'rb', check_sq=False)
            for read in bam_data.fetch(until_eof=True):
                sequences.append(read.seq)
                qualities.append(convert_char_to_phred_score(read.qual))
    else:
        raise ValueError(f'The file path "{file_path}" should be a FASTQ or BAM file.')

    reads_df = pd.DataFrame({'Sequence': sequences, 'Phred scores': qualities})
    return reads_df


def average_phred_score(phred_scores):
    average_score = sum(phred_scores) / len(phred_scores) if phred_scores else 0

    return average_score


def plot_length_phred_scores(file_path, output_file='read_quality_length.html', template='plotly_white'):
    reads_df = load_reads(file_path)
    reads_df['Average phred score'] = reads_df['Phred scores'].apply(average_phred_score)
    reads_df['Sequence length'] = reads_df['Phred scores'].apply(len)
    fig = px.density_contour(reads_df,
                             x='Average phred score',
                             y='Sequence length',
                             marginal_x='histogram',
                             marginal_y='histogram',
                             template=template)
    fig.write_html(output_file)


def plot_phred_scores(file_path,
                      output_file='read_quality.html',
                      template='plotly_white',
                      n_sample=1000,
                      nb_long_seq_filter=100):
    reads_df = load_reads(file_path)
    reads_df['Length'] = reads_df['Phred scores'].apply(len)
    max_length = reads_df['Length'].max()
    reads_df.loc[reads_df['Length'] < np.sort(
        reads_df['Length'])[-nb_long_seq_filter - 1]]  #remove sequences that have less than x sequences longer
    reads_df['Phred score padded'] = reads_df['Phred scores'].apply(lambda x: x + [np.nan] * (max_length - len(x)))
    phred_scores_sample = np.array(reads_df['Phred score padded'].sample(n=n_sample, random_state=1).tolist())
    phred_scores_sample = np.concatenate(
        (phred_scores_sample, np.expand_dims(np.arange(1, phred_scores_sample.shape[1] + 1), axis=0)))
    non_nan_counts = np.sum(~np.isnan(phred_scores_sample), axis=0)
    columns_to_keep = non_nan_counts >= 100
    phred_scores_sample = phred_scores_sample[:, columns_to_keep]
    quantiles = np.nanquantile(phred_scores_sample[:-1, :], axis=0, q=[0, 0.25, 0.5, 0.75, 1])
    mean = np.nanmean(phred_scores_sample[:-1, :], axis=0)
    positions = phred_scores_sample[-1, :]

    fig = go.Figure()
    shapes = [{
        'type': 'rect',
        'xref': 'paper',
        'yref': 'y',
        'x0': 0,
        'x1': 1,
        'y0': -1e9,
        'y1': 12,
        'fillcolor': 'red',
        'opacity': 0.2,
        'line': {
            'width': 0
        }
    }, {
        'type': 'rect',
        'xref': 'paper',
        'yref': 'y',
        'x0': 0,
        'x1': 1,
        'y0': 12,
        'y1': 20,
        'fillcolor': 'yellow',
        'opacity': 0.2,
        'line': {
            'width': 0
        }
    }, {
        'type': 'rect',
        'xref': 'paper',
        'yref': 'y',
        'x0': 0,
        'x1': 1,
        'y0': 20,
        'y1': 50,
        'fillcolor': 'green',
        'opacity': 0.2,
        'line': {
            'width': 0
        }
    }]
    fig.update_layout(shapes=shapes, yaxis=dict(range=[0, 50], fixedrange=True), template=template, showlegend=False)
    fig.add_trace(
        go.Scatter(x=np.concatenate([positions, positions[::-1]]),
                   y=np.concatenate([quantiles[1], quantiles[3][::-1]]),
                   fill='toself',
                   fillcolor='lightblue',
                   opacity=0.5,
                   line=dict(color='lightblue'),
                   hoverinfo='skip',
                   showlegend=False))

    fig.add_trace(
        go.Scatter(x=positions, y=quantiles[1], mode='lines', name='25th percentile', line=dict(color='skyblue')))
    fig.add_trace(go.Scatter(x=positions, y=quantiles[2], mode='lines', name='Median', line=dict(color='skyblue')))
    fig.add_trace(
        go.Scatter(x=positions, y=quantiles[3], mode='lines', name='75th percentile', line=dict(color='skyblue')))
    fig.add_trace(go.Scatter(x=positions, y=mean, mode='lines', name='Average', line=dict(color='grey')))
    fig.write_html(output_file)


def plot_phylogeny_pie_chart_krona(data, output_file='pie_charts.html'):
    phylo = ['kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    data = data.copy()
    data = data[data['keep_for_analysis'] == True]
    data = data[['sample_barcode', 'nb_reads'] + phylo]
    data = data.fillna(' ')

    sample_grouped = data.groupby('sample_barcode')
    with open('all_samples.tsv', 'w') as handle_all:
        for sample_name, sample_group in sample_grouped:
            sample_group_conssensus = sample_group[sample_group['nb_reads'] > 1]
            sample_group_no_group = sample_group[sample_group['nb_reads'] == 1]
            with open(f'{sample_name}_consenssus.tsv', 'w') as handle_conssensus:
                handle_conssensus.write(sample_name + '\n')
                handle_all.write(sample_name + '\n')
                phylo_grouped = sample_group_conssensus.groupby(phylo)
                for _, phylo_group in phylo_grouped:
                    row = f'{sum(phylo_group["nb_reads"])}'
                    phylo_group = phylo_group.iloc[0]
                    for p in phylo:
                        row += '\t'
                        row += str(phylo_group[p])
                    handle_conssensus.write(row + '\n')
                    handle_all.write(row + '\n')
            if len(sample_group_no_group):
                with open(f'{sample_name}_nogroup.tsv', 'w') as handle_no_group:
                    handle_no_group.write(sample_name + '\n')
                    phylo_grouped = sample_group_no_group.groupby(phylo)
                    for _, phylo_group in phylo_grouped:
                        row = f'{sum(phylo_group["nb_reads"])}'
                        phylo_group = phylo_group.iloc[0]
                        for p in phylo:
                            row += '\t'
                            row += str(phylo_group[p])
                        handle_no_group.write(row + '\n')
                        handle_all.write(row + '\n')

    os.system(f'ktImportText *.tsv -o {output_file}')
    patterns = ['*_nogroup.tsv', '*_conssensus.tsv', 'all_samples.tsv']
    for pattern in patterns:
        for file in glob.glob(pattern):
            os.remove(file)
