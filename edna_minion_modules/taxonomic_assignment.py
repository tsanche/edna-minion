import os
import logging
import concurrent.futures
from edna_minion_modules.utils import execute_in_directory
from edna_minion_modules.utils import run_command


class Taxonomic_assignment:

    def __init__(self,
                 run_name: str,
                 run_path: str,
                 clustering_path: str,
                 max_workers: int,
                 process_unclustered_read: bool,
                 arguments: dict,
                 skip_step: bool = False,
                 taxonomic_assignment_path: str = 'taxonomic_assignment') -> None:

        logging.info(f'Preparing taxonomic assignment')
        self.run_name = run_name
        self.run_path = run_path
        self.taxonomic_assignment_path = taxonomic_assignment_path
        self.skip_step = skip_step
        if self.skip_step:
            logging.info('Skipping taxonomic assignment')
        else:
            self.max_workers = max_workers
            arguments['db'] = os.path.realpath(arguments['db'])

            with execute_in_directory(self.run_path):
                clustering_folder = [f.name for f in os.scandir(clustering_path) if f.is_dir()]
                clustering_paths = [
                    os.path.join(clustering_path, f, f"{f}_consensussequences.fasta") for f in clustering_folder
                    if os.path.exists(os.path.join(clustering_path, f, f"{f}_consensussequences.fasta"))
                ]
                if process_unclustered_read:
                    clustering_paths = clustering_paths + [
                        os.path.join(clustering_path, f, f"{f}_nogroup_unique.fasta") for f in clustering_folder
                        if os.path.exists(os.path.join(clustering_path, f, f"{f}_nogroup_unique.fasta"))
                    ]
                if not os.path.exists(self.taxonomic_assignment_path):
                    logging.info(f'Creating taxonomic assigment output path in "{self.taxonomic_assignment_path}"')
                    os.makedirs(self.taxonomic_assignment_path)

            arguments_string = ' '.join([f'-{arg} {arguments[arg]}' for arg in arguments])
            self.assignment_commands = [
                f'blastn {arguments_string} -query {f} -out {os.path.join(self.taxonomic_assignment_path, os.path.basename(f).split(".")[0])}.tsv'
                for f in clustering_paths
            ]

    def run(self) -> str:
        with execute_in_directory(self.run_path):
            if self.skip_step:
                logging.info(f'Using taxonomic assigned data in "{self.taxonomic_assignment_path}"')
            else:
                logging.info('Starting taxonomic assignment')
                with concurrent.futures.ThreadPoolExecutor(max_workers=self.max_workers) as executor:
                    futures = [executor.submit(run_command, cmd) for cmd in self.assignment_commands]
                    for future in concurrent.futures.as_completed(futures):
                        result = future.result()
                        if len(result[0]) > 0:
                            logging.info(result[0].decode('utf-8'))
                        if len(result[1]) > 0:
                            logging.error(result[1].decode('utf-8'))

        return self.taxonomic_assignment_path
