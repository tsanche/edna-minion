import time
import logging
import coloredlogs
import argparse
import edna_minion_modules.basecalling as basecalling
import edna_minion_modules.demultiplexing as demultiplexing
import edna_minion_modules.clustering as clustering
import edna_minion_modules.taxonomic_assignment as taxonomic_assignment
import edna_minion_modules.reporting as reporting
import edna_minion_modules.utils as utils
from datetime import datetime

coloredlogs.install(level='DEBUG')


def main():
    parser = argparse.ArgumentParser(description='eDNA minion pipeline')
    optional_arg = parser._action_groups.pop()
    required_arg = parser.add_argument_group('required arguments')
    parser._action_groups.append(optional_arg)
    required_arg.add_argument('-c',
                              '--config_file',
                              type=str,
                              required=True,
                              help='path to the configuration file (in toml format) to be used')
    optional_arg.add_argument('-r',
                              '--resume',
                              action=argparse.BooleanOptionalAction,
                              default=False,
                              help='set to true to overwrite or continue an existing run')

    args = parser.parse_args()
    resume_value = getattr(args, 'resume')

    start_time = time.time()
    start_date = datetime.now()
    logging.info(f'Workflow started at: {start_date.strftime("%Y-%m-%d %H:%M:%S")}')

    run_folder, run_path, config = utils.load_config(args.config_file, resume_value,
                                                     start_date.strftime("%Y-%m-%d_%H_%M_%S"))

    basecalling_file_path = basecalling.Basecalling(run_folder, run_path, **config['basecalling']).run()
    demultiplexing_path, barcode_table_file = demultiplexing.Demultiplexing(run_folder, run_path, basecalling_file_path,
                                                                            **config['demultiplexing']).run()
    clustering_path = clustering.Clustering(run_folder, run_path, demultiplexing_path, **config['clustering']).run()
    taxonomic_assignment_path = taxonomic_assignment.Taxonomic_assignment(run_folder, run_path, clustering_path,
                                                                          **config['taxonomic_assignment']).run()
    reporting.Reporting(run_folder, run_path, barcode_table_file, clustering_path, taxonomic_assignment_path,
                        config['phylogenetic_thresholds'], config['taxonomic_assignment']['process_unclustered_read'],
                        config['taxonomic_assignment']['arguments']['db'],
                        config['taxonomic_assignment']['arguments']['outfmt'],
                        config['taxonomic_assignment']['arguments']['max_target_seqs'],
                        config['reference_database']['format'], config['reference_database']['separator']).run()

    logging.info(f'Pipeline completed after {time.time() - start_time:.2f} seconds')


if __name__ == "__main__":
    main()
